import { PruebaETS2Page } from './app.po';

describe('prueba-ets2 App', () => {
  let page: PruebaETS2Page;

  beforeEach(() => {
    page = new PruebaETS2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
