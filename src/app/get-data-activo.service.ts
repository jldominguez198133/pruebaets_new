import { Injectable } from '@angular/core';
import { Headers, RequestOptions, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class HTTPDataActivo {
  constructor (private _http: Http) {}

  getDataActivo(id) {
    let opt: RequestOptions;
    let myHeaders: Headers = new Headers;
    myHeaders.append('Content-type', 'application/json');
    myHeaders.append('JsonStub-User-Key', '9facef2e-9583-4a83-9f08-c87159f1c113');
    myHeaders.append('JsonStub-Project-Key', '6ed070c1-b334-4612-8fa8-169c5e45baef');
    opt = new RequestOptions({
      headers: myHeaders
    });

    return this._http.get('http://jsonstub.com/etsfintech/symbols/' + id, opt)
      .toPromise()
      .then(respuesta => respuesta.json())
      .catch(this.ocurrioUnError)
  }

  private ocurrioUnError(error:any) {
    console.log("Ocurrió un error en la llamada HTTP", error);
    return Promise.reject(error.message || error);
  }
}
