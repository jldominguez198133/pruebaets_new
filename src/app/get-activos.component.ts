import { Component, OnInit } from '@angular/core';
import { HTTPActivosService } from './get-activos.service';
import { HTTPDataActivo } from './get-data-activo.service';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

console.clear();

@Component({
  selector: 'get-activos',
  templateUrl: './get-activos.component.html',
  styleUrls: ['./get-activos.component.scss'],
  providers: [HTTPActivosService, HTTPDataActivo]
})
export class ActivosComponent implements OnInit{
  title = 'Aquí van los Activos';
  getData: Object;
  getDataActivo: Object;
  openClassBool = false;
  closeSpinnerClassBool = false;
  valor1: string = '';
  valor2: string = '';

  constructor (private _httpActivos: HTTPActivosService, private _httpDataActivo: HTTPDataActivo) {}

  // Cuando los filtros select cambian
  onChange(newValor) {
    this.valor1 = newValor;
    this.valor2 = newValor;
    console.log(this.valor1)
    if (this.valor1 != '') {
      this.onActivosGet(this.valor1);
    }
    if (this.valor2 != '') {
      this.onActivosGet(this.valor2);
    }
  }

  // Añadimos clases a elementos
  addSpinnerClass() {
    this.closeSpinnerClassBool = true;
  }

  addClass() {
    this.openClassBool = true;
  }

  // Removemos clases de elementos
  removeClass() {
    this.openClassBool = false;
  }

  removeSpinnerClass() {
    this.closeSpinnerClassBool = false;
  }

  onActivosGet(valor?) {
    this._httpActivos.getActivos()
      .subscribe((respuesta) => {
        this.getData = respuesta;
        console.log(this.getData, valor)
      });
  }

  ngOnInit() {
    this.onActivosGet();
  }

  clickFicha(id) {
    console.log(id);

    this.addClass();
    this.removeSpinnerClass();

    let activoData = this._httpDataActivo.getDataActivo(id)
      .then(respuesta => {
        this.getDataActivo = respuesta;
        this.addSpinnerClass();
      });


    console.log(activoData)
  }

}
