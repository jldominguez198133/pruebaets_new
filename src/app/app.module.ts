import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';

import { CommentModule } from './Comments/comments.module';

import { AppComponent } from './app.component';
import { ActivosComponent } from './get-activos.component';
import { EmitterService }          from './emitter.service';
import { HTTPActivosService }          from './get-activos.service';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BaseChartDemoComponent } from './chart.component';

@NgModule({
  declarations: [
    AppComponent,
    ActivosComponent,
    BaseChartDemoComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    CommentModule,
    ChartsModule
  ],
  providers: [
    EmitterService,
    HTTPActivosService
  ],
  bootstrap: [AppComponent, ActivosComponent, BaseChartDemoComponent]
})
export class AppModule { }
